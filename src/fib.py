import os
import ConfigParser
import subprocess
import socket
from subprocess import call
from shutil import rmtree
from time import sleep
DETACHED_PROCESS = 0x00000008

fuse_serers = ""
fuse_project = ""
fuseCommands = []
threads = 1
checking_period = 2
should_open_plink = 'yes'
maven_command_agaist_project = ''


def main():
    setupVariables()
    showVariables()
    mvn( maven_command_agaist_project , fuse_project)
    prepareFuseServer()
    startFuseServer()
    installCommand()
    if should_open_plink == 'yes':
        openPutty()    
    

def setupVariables():
    global fuse_serers  , fuse_project, threads, fuseCommands, checking_period , should_open_plink , maven_command_agaist_project
    config = ConfigParser.RawConfigParser()
    config.read('fib.cfg')
    fuse_serers = config.get('userSetting', 'fuse_server')
    fuse_project = config.get('userSetting', 'fuse_project')
    checking_period = config.getfloat('optional', 'fuse_checking_privisioning_period')
    should_open_plink = config.get('postInstall','open_plink_after_install')
    maven_command_agaist_project = config.get('mvnCommand','maven_command_agaist_project')
    fuseCommands = config.items('fuseCommands')


def showVariables():
    print fuse_serers
    print fuse_project
    print fuseCommands

def mvn(command , project_path):
    print 'mvn ' + command + ' -f ' + project_path
    call('mvn ' + command + ' -f ' + project_path , shell=True)
    

def prepareFuseServer():
    if isFuseServerRunning():
        print "Fuse is running...."
        stopFuseServer()
    while True:
        if not isFuseServerRunning():
            _is_data_deleted = False
            while True:
                try:
                    if os.path.isdir(fuse_serers + "\\data\\"):
                        rmtree(fuse_serers + "\\data\\")
                    _is_data_deleted = True
                    break
                except:
                    print "gracefully stopping fuse server... "
                    sleep(checking_period)
                    continue
            if _is_data_deleted:
                break
        sleep(checking_period)
        

# Fuse status report running.. even a server is provisioning. When checking 
# the server's state if it ready for the next task: checking port fuse port 
# is more appropriate. 
def isFuseServerRunning():    
    return isPortOpen('localhost', 8101)
    
def isPortOpen(host, port):
    _socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    return_code = _socket.connect_ex((host, port))
    return return_code == 0 
    
def stopFuseServer():
    print "Stopping fuse..."
    call(fuse_serers + "\\bin\\stop.bat", shell=True)    
        
        
def startFuseServer():
    # pid = subprocess.Popen([fuse_serers+"\\bin\\fuse.bat","server"]).pid
    pid = subprocess.Popen([fuse_serers + "\\bin\\fuse.bat", "server"], creationflags=DETACHED_PROCESS).pid
    print "Fuse start with process id = " + str(pid)
    
    while True:
        print "Fuse is starting..."
        if isFuseServerRunning():
            print "Fuse is ready"
            break
        sleep (checking_period)
    
    
    
def installCommand():
    print "Provisioning fuse with the following commands"
    for i in fuseCommands : print i
    print "------------------------"
    print "Starting execute fuse commands"
    for i in fuseCommands : sshToFuse(i)
    

def sshToFuse(command):
    plink = os.getcwd() +'\\tools\\plink.exe'   
    print "Executing task " + command[0] 
      
    output = subprocess.call(plink + ' -P 8101 localhost -l admin -pw admin -C "' + command[1] + '"'   , shell=True)
    print "Success" if output == 0 else "Fail"

def openPutty():
    putty = os.getcwd() +'\\tools\\putty.exe'
    subprocess.call(putty + ' -P 8101 localhost -l admin -pw admin', shell=True)
    

if __name__ == '__main__':  
    main()  

    
